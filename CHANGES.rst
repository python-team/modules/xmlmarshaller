History
=======

1.0.1 (2018-11-12)
------------------

- Fix changelog.

1.0 (2018-11-12)
----------------

- Stop distinguish unicode and bytes and always return 'str'

0.10 (2018-09-12)
-----------------

- Add support for Python 3

0.9.7 (2010-10-30)
------------------

- Enhance egg folder structure
  [nicolas Delaby]
- Improve tests
  [nicolas Delaby]
- add XSD Schema
  [nicolas Delaby]

0.9.6 (2010-10-12)
------------------

- [fix] Support boolean transformation
  [Nicolas Delaby]

0.9.5 (2010-09-01)
------------------

- [fix] Formatting of documentation
  [Lukasz Nowak]

0.9.4 (2010-09-01)
------------------

- [fix] Instances are now correctly unmarshalled.
  [Cedric de Saint Martin]
